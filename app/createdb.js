const pg = require('pg');
process.env.PGDATABASE = 'app';
process.env.PGUSER = 'postgres';
process.env.PGPASSWORD = '2878';
const pool = new pg.Pool()

pool.on('error', (err, conn) => {
  console.log(err.stack);
  process.exit(-1);
});

pool.connect((err, conn, done) => {
  if (err) throw err;
  const q = 'create table users (		' +
	    'username varchar(255) primary key,	' +
	    'password varchar(255) not null,	' +
	    'admin boolean,			' +
	    'color char(6)			' +
	    ');					';
  conn.query(q, (err) => {
    if (err) throw err;
    insertUser(conn, done);
  });
});

function insertUser(conn, done) {
  const q = "insert into users (username, password, admin, color)" +
	    "values ('fred', '1234', 't', '0000FF')," +
	    "       ('david', '2345', 'f', '0000FF');";
  conn.query(q, (err) => {
    if (err) throw err;
    done();
    pool.end();
  });
}
