const http      = require('http');
const qs        = require('querystring');
const express   = require('express');
const stringify = require('stringify');
const auth      = require('./auth');
const engine    = require('./engine');
const sessions  = require('./sessions');
const db        = require('./db');

const port = 8000;

const app              = express();
const static           = express.static('public');
const urlencodedParser = express.urlencoded({ extended: false });

app.use(static);
app.set('public', './public');
app.use(auth);

app.engine('html', engine);
app.set('views', './views');

app.get('/users_list.html', adminCheck, urlencodedParser, (req, res) => {
  var table = '';
  var reo ='<html><body>{${table}}</body></html>';
  const username = req.session.username;
  db.getUserList(username, (result) => {
    for(var i = 0; i < result.length; i++){
      table +='<tr><td>'  + result[i].username + 
              '</td><td>' + result[i].password + 
              '</td><td>' + result[i].admin + 
              '</td><td>' + result[i].color + 
              '</td></tr>';
    }
    var table1 = '<table border="1"><tr>' +
                 '<th>Username</th>'      +
                 '<th>Password</th>'      +
                 '<th>Admin</th>'         +
                 '<th>Color</th>'         +
                 '</tr>' + table          + 
                 '</table>';
  reo = reo.replace('{${table}}', table1);
  res.writeHead(200, {'Content-Type':'text/html; charset=utf-8'});
  res.write(reo, 'utf-8');
  res.write('<br><form action="/" method="get"><input type="submit" value="Return to Homepage" /></form>');
  res.end();    
  });
});

app.get('/', function(req, res) {
  const username = req.session.username;
  db.adminChk(username, (Achk) => {
    if (Achk) {
      db.getUserColor(username, (color) => {
        const params =  {color: color, msg1: 'none', msg2: 'none', msg3: 'none'};
        res.render('admin_home.html', {params: params});      
      });
    } else {
      db.getUserColor(username, (color) => {
        const params =  {user: username, color: color, msg1: 'none', msg2: 'none', msg3: 'none'};
        res.render('home.html', {params: params});
      });
    }
  });
});

app.post('/color', urlencodedParser, (req, res) => {
  const username = req.session.username;
  const color = req.body.color;
  db.setUserColor(username, color, () => {
    res.redirect('/');
  }); 
});

app.post('/create', adminCheck, urlencodedParser, (req, res) => {
  const username = req.session.username;
  const newuser = req.body.newuser;
  const newpassword = req.body.newpassword;
  const password_verify = req.body.password_verify;
  if (newpassword !== password_verify) {
    const params = {msg1: 'inline', msg2: 'none'};
    res.render('ct_acct.html', {params: params});
  } else {
    db.chkUser(newuser, (user1) => {
      if (newuser === user1) {
      const params = {msg1: 'none', msg2: 'inline'};
      res.render('ct_acct.html', {params: params});      
      } else {
        db.createUser(newuser, newpassword, () => {
          db.getUserColor(username, (color) => {
            const params = {color: color, msg1: 'inline', msg2: 'none', msg3: 'none'};
            res.render('admin_home.html', {params: params});
          });
        });
      }
    });
  }
});

app.post('/changepwd', adminCheck, urlencodedParser, (req, res) => {
  const username = req.session.username;
  const user = req.body.user;
  const newpassword = req.body.newpassword;
  const new_password_verify = req.body.password_verify;
  if (newpassword !== new_password_verify) {
    const params = {msg1: 'inline', msg2: 'none'};
    res.render('cng_pwd.html', {params: params});
  } else {
    db.chkUser(user, (user1) => {
      if (user !== user1) {
        const params = {msg1: 'none', msg2: 'inline'};
        res.render('cng_pwd.html', {params: params});      
      } else {
        db.setUserPwd(user, newpassword, () => {
          db.getUserColor(username, (color) => {
            const params = {color: color, user: user, msg1: 'none', msg2: 'none', msg3: 'inline'};
            res.render('admin_home.html', {params: params});
          });
        });
      }
    });
  }
});


app.post('/delete', adminCheck, urlencodedParser, (req, res) => {
  const username = req.session.username;
  const deluser = req.body.deluser
  db.chkUser(deluser, (user1) => {
    if (deluser !== user1) {
      const params = {msg1: 'inline'};
      res.render('dl_acct.html', {params: params});      
    } else {
      db.deleteUser(deluser, () => {
        db.getUserColor(username, (color) => {
          const params = {color: color, msg1: 'none', msg2: 'inline', msg3: 'none'};
          res.render('admin_home.html', {params: params});
        });
      });
    }
  });
});

app.get('/color.html', function(req, res) {
  const username = req.session.username;
  db.getUserColor(username, (color) => {
    const params =  { 
      color: color, 
      red  : '', 
      green: '', 
      blue : ''
    }; 
    if       (color === 'FF0000') params.red   = 'checked';
    else if  (color === '00FF00') params.green = 'checked';
    else if  (color === '0000FF') params.blue  = 'checked';
    res.render('color.html', {params: params});
  });
});

app.get('/ct_acct.html', function (req, res) {
  const username = req.session.username;
  const params = {msg1: 'none', msg2: 'none'};
  res.render('ct_acct.html', {params: params});
});

app.get('/cng_pwd.html', function (req, res) {
  const username = req.session.username;
  const params = {msg1: 'none', msg2: 'none'};
  res.render('cng_pwd.html', {params: params});
});

app.get('/dl_acct.html', function (req, res) {
  const username = req.session.username;
  const params = {msg1: 'none'};
  res.render('dl_acct.html', {params: params});
});

function adminCheck(req, res, next) {
  const username = req.session.username;
  db.adminChk(username, (admin_approved) => {
    if (admin_approved) {
      next();
    } else {
      res.end('Permission denied');
    }
  });
};

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});

