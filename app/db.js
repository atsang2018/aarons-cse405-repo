const pg = require('pg');
process.env.PGDATABASE = 'app';
process.env.PGUSER = 'postgres';
process.env.PGPASSWORD = '2878';
const pool = new pg.Pool()

pool.on('error', (err, conn) => {
  console.log(err.stack);
  process.exit(-1);
});

function getUserList(username, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "select * from users";
    const params = [];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
        cb(null);
      } else if (result.rows.length === 0) {
        cb(null);
      } else {
        cb(result.rows);
      }
      done();
    });
  });
}

function getUser(username, password, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "select color from users " +
              "where username=$1::text and password=$2::text";
    const params = [username, password];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
        cb(null);
      } else if (result.rows.length === 0) {
        cb(null);
      } else {
        const row = result.rows[0];
        cb({
          color: row['color']
        });
      }
      done();
    });
  });
}

function chkUser(username, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "select username from users where username=$1::text";
    const params = [username];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
        cb(null);
      } else if (result.rows.length === 0) {
        cb(null);
      } else {
        const row = result.rows[0];
        cb(row['username']);
      }
      done();
    });
  });
}

function getUserColor(username, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "select color from users where username=$1::text";
    const params = [username];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
        cb(null);
      } else if (result.rows.length === 0) {
        cb(null);
      } else {
        const row = result.rows[0];
        cb(row['color']);
      }
      done();
    });
  });
}

function setUserColor(username, color, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "update users set color=$1::text where username=$2::text";
    const params = [color, username];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
      } 
      done();
      cb();
    });
  });
}

function createUser(username, password, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "insert into users values ($1::text, $2::text, 'f', '0000FF')";
    const params = [username, password];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
      }
      done();
      cb();
    });
  });
}

function setUserPwd(username, password, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "update users set password=$1::text where username=$2::text";
    const params = [password, username];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
      } 
      done();
      cb();
    });
  });
}

function deleteUser(username, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "delete from users where username=$1::text";
    const params = [username];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
      } 
      done();
      cb();
    });
  });
}

function adminChk(username, cb) {
  pool.connect((err, client, done) => {
    if (err) throw err;
    const q = "select admin from users where username=$1::text";
    const params = [username];
    client.query(q, params, (err, result) => {
      if (err) {
        console.log(err.stack);
        cb(null);
      } else if (result.rows.length === 0) {
        cb(null);
      } else {
        const row = result.rows[0];
        cb(row['admin']);
      }
      done();
    });
  });
}

exports.getUserList = getUserList;
exports.getUser = getUser;
exports.chkUser = chkUser;
exports.getUserColor = getUserColor;
exports.setUserColor = setUserColor;
exports.createUser = createUser;
exports.setUserPwd = setUserPwd;
exports.deleteUser = deleteUser;
exports.adminChk = adminChk;

