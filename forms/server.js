const http = require('http');
const st = require('st');
const qs = require('querystring');

const port = 8000;

var mount = st({
	path: 'public/',
	index: 'index.html'
});

const server = http.createServer((req, res) => {
	if (req.url === '/test') {
		handleForm(req, res)
	} else {
	mount(req, res);
	}
});

server.listen(port, () => {
        console.log(`Server running at http://localhost:${port}/`);
});

function handleForm(req, res) {
  console.log('\nForm data received:\n');
  console.log(req.headers);
  console.log('\n');

  let body = '';
  req.on('data', (chunk) => {
    body += chunk.toString('utf8');
  });
  
  req.on('end', () => {
    const form = qs.parse(body);
	res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
	res.write('<p>Hello, ' + form.firstname + ' ' + form.lastname + '</p>\n');
	res.write('<p>Flavors: ' + form.Flavors + '</p>\n');
	res.write('<p>Size: ' + form.Size + '</p>\n');    
	res.end();
  });
}
