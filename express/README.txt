views folder -- holds html template files.
auth.js -- code for login.html file and authenticating users
createdb.js -- script to create table in postgresql database and fill with test data
db.js -- file containing postgresql queries used by code in server.js file
engine.js -- code to replace keys in template files with relavent data 
server.js -- main node.js server code
sessions.js -- code to create unique sessions and cookies 
