create table Student (
        student_id integer primary key,
        fName varchar(15),
	lName varchar(15),
	phoneNo varchar(12),
	address varchar(30),
	city varchar(15),
	state varchar(2),
	zipCode integer
);

insert into Student values (11223344, 'John', 'Smith', '123-456-7890', '2233 Main St.', 'Los Angeles', 'CA', 22334);
insert into Student values (22223344, 'Mary', 'Olson', '345-123-7795', '1234 Melrose Blvd.', 'Los Angeles', 'CA', 12233);
insert into Student values (33334455, 'Rick', 'Mann', '410-345-2355', '3499 Florida Ave.', 'Orlando', 'FL', 55599);

select * from Student;

select * from Student where student_id=22223344;

update Student set lName = 'Umbra' where student_id=33334455;

select * from Student;

delete from Student where student_id=22223344;

select * from Student;
