// Initialize

const pg = require('pg');
process.env.PGDATABASE = 'pg';
process.env.PGUSER = '004991216@csusb.edu';
process.env.PGPASSWORD = '2878';
const pool = new pg.Pool()

pool.on('error', (err, conn) => {
	console.log(err.stack);
	process.exit(-1);
});

//Utility Functions

function insertRow(conn, student_Id, fName, lName, phone_No, address, city, state, zip_Code, cb) {
	conn.query("insert into Student values ($1, $2::text, $3::text, $4::text, $5::text, $6::text, $7::text, $8)", [student_Id, fname, lname, phone_No, address, city, state, zip_Code], (err) => {
		if (err) throw err; else cb();
	});
}

function selectAll(conn, cb) {
	conn.query("select * from Student", (err, result) => {
		if (err) throw err;
		console.log("\nselect * from Student");
		result.rows.forEach((row) => {
			console.log(row.student_Id + ' ' + row.fName + ' ' + row.lName + ' ' + row.phone_No + ' ' + row.address + ' ' + row.city + ' ' + row.state + ' ' + row.zip_Code);
		});
		cb();
	});
}

function selectById(conn, student_Id, cb) {
	conn.query("select * from Student where student_Id=$1", [student_Id], (err, result) => {
		if (err) throw err;
		console.log("\nselect * from Student where student_Id = ?");
		result.rows.forEach((row) => {
			console.log(row.student_Id + ' ' + row.fName + ' ' + row.lName + ' ' + row.phone_No + ' ' + row.address + ' ' + row.city + ' ' + row.state + ' ' + row.zip_Code);
		});
		cb();
	});
}

function updateRow(conn, studentid, lName, cb) {
	conn.query("update Student set lName = $1::text where student_Id = $2" , [lName, student_Id], (err) => {
		if (err) throw err;
		cb();
	});
}

function deleteRow(conn, student_Id, cb) {
	conn.query("delete from Student where student_Id = $1", [student_Id], (err) => {
		if (err) throw err;
		cb();
	});
}

//Main Functions

// Get a database connection from the pool.
pool.connect((err, conn, done) => {
	if (err) throw err;
	createTable(conn, done);
});

// Create table Student.
function createTable(conn, done) {
	const q = 'create table Student (	' +
		'student_Id integer primary key,' +
	        'fName varchar(15),		' +
		'lName varchar(15),		' +
		'phone_No varchar(12),		' +
		'address varchar(30),		' +
		'city varchar(15),		' +
		'state varchar(2),		' +
		'zip_Code integer		' +
		')				';
	conn.query(q, (err) => {
		if (err) throw err; else insertA(conn, done);
	});
}

function insertA(conn, done) {
	insertRow(conn, 11223344, 'John', 'Smith', '123-456-7890', '2233 Main St.', 'Los Angeles', 'CA', 22334, () => {
		insertB(conn, done);
	});
}

function insertB(conn, done) {
	insertRow(conn, 22223344, 'Mary', 'Olson', '345-123-7795', '1234 Melrose Blvd.', 'Los Angeles', 'CA', 12233, () => {
		insertC(conn, done);
	});
}

function insertC(conn, done) {
	insertRow(conn, 33334455, 'Rick', 'Mann', '410-345-2355', '3499 Florida Ave.', 'Orlando', 'FL', 55599, () => {
		selectB(conn, done);
	});
}

function selectB(conn, done) {
	selectById(conn, 22223344, () => {
		updateB(conn, done);
	});
}

function updateB(conn, done) {
	updateRow(conn, 33334455, 'Umbra', () => {
		SelectAll1(conn, done);
	});
}

function SelectAll1(conn, done) {
	selectAll(conn, () => {
		deleteB(conn, done);
	});
}

function deleteB(conn, done) {
	deleteRow(conn, 22223344, () => {
		selectAll2(conn, done);
	}); 
}

function selectAll2(conn, done) {
	selectAll(conn, () => {
		myEnd(conn, done);
	});
}

function myEnd(conn, done) {
	done();
	pool.end();
}
