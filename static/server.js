const http = require('http');
const st = require('st');
const fs = require('fs');
const url = require('url');

const port = 8000;

var mount = st({
  path: 'public/',
  index: 'index.html'
});

const server = http.createServer((req, res) => {
  if (req.url === '/hi') {

  fs.readFile('./public/index2.html', function(err, data) {
	res.end(data);
  });
  } else {
	mount(req, res);
}
});

server.listen(port, () => {
        console.log(`Server running at http://localhost:${port}/`);
});
