var http = require('http');

const html =
  "<html>"                           +
  "<head><title>405</title></head>"  +
  "<body><h1>Hello Aaron!</h1></body>"         +
  "</html>";

http.createServer(function(req, res){
  res.writeHeader(200, {'Content-Type': 'text/html'});
  res.write(html);
  res.end();
}).listen(8000);

console.log('Server running at http://localhost:8000/');
